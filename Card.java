public class Card {
	
	private String suit;
	private String value;
	
	// Constructor
	public Card(String suit, String value) {
		
		this.suit = suit;
		this.value = value;
		
	}
	
	// Getters for the suit and value
	public String getSuit() {
		
		return this.suit;
		
	}
	
	public String getValue() {
		
		return this.value;
		
	}
	
	// Returns the value and the suit
	public String toString() {
		
		if(Integer.parseInt(this.value) == 1) {
			
			return "Ace of " + this.suit;
			
		}
		if(Integer.parseInt(this.value) == 11) {
			
			return "Jack of " + this.suit;
			
		}
		if(Integer.parseInt(this.value) == 12) {
			
			return "Queen of " + this.suit;
			
		}
		if(Integer.parseInt(this.value) == 13) {
			
			return "King of " + this.suit;
			
		}
		
		return this.value + " of " + this.suit;
		
	}
	
	// Calculates the score based on the suit and value
	public double calculateScore() {
		
		double score = Integer.parseInt(this.value);
		
		if(this.suit.equals("Spades")) {
			
			return score += 0.1;
			
		}
		else if(this.suit.equals("Clubs")) {
			
			return score += 0.2;
			
		}
		else if(this.suit.equals("Diamonds")) {
			
			return score += 0.3;
			
		}
		
		return score += 0.4;
		
	}
	
}