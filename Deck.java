import java.util.Random;

public class Deck {
	
	private Card[] cards; 
	private int numberOfCards;
	private Random rng;
	
	// Initializes the deck and gives all the cards a value and a suit
	public Deck() {
		
		numberOfCards = 52;
		cards = new Card[numberOfCards];
		rng = new Random();
		
		String[] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
		int cardsPerSuit = 13;
		String[] value = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
		
		for(int i = 0; i < numberOfCards; i++) {
			
			int suitIndex = i / cardsPerSuit;
			
			int valueIndex = i % 13;
			
			this.cards[i] = new Card(suits[suitIndex], value[valueIndex]);
			
		}
		
	}
	
	// Returns the number of cards left available
	public int length() {
		
		return this.numberOfCards;
		
	}
	
	// Returns the card at the top (last position) of the deck array
	public Card drawTopCard() {
		
		Card lastCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return lastCard;
		
	}
	
	// Returns all the cards with their values and suits
	public String toString() {
		
		String cardString = "";
		
		for(int i = 0; i < this.numberOfCards; i++) {
			cardString += this.cards[i] + "\n";
		}
		
		return cardString;
		
	}
	
	// Randomizes the position of cards in the deck
	public void shuffle() {
		
		for(int i = 0; i < this.numberOfCards; i++) {
			
			int randPos = rng.nextInt(this.numberOfCards - i) + i;
			Card cardToSwap = this.cards[i];
			this.cards[i] = this.cards[randPos];
			this.cards[randPos] = cardToSwap;
			
		}
		
	}
	
}